﻿namespace Arcanoid
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.timer_time = new System.Windows.Forms.Timer(this.components);
            this.playground = new System.Windows.Forms.Panel();
            this.scr_lbl = new System.Windows.Forms.Label();
            this.your_scr_lbl = new System.Windows.Forms.Label();
            this.win_lbl = new System.Windows.Forms.Label();
            this.Sorry_lbl = new System.Windows.Forms.Label();
            this.Menu = new System.Windows.Forms.Button();
            this.New_gm = new System.Windows.Forms.Button();
            this.wall_15 = new System.Windows.Forms.PictureBox();
            this.wall_14 = new System.Windows.Forms.PictureBox();
            this.wall_13 = new System.Windows.Forms.PictureBox();
            this.wall_12 = new System.Windows.Forms.PictureBox();
            this.wall_11 = new System.Windows.Forms.PictureBox();
            this.wall_10 = new System.Windows.Forms.PictureBox();
            this.wall_9 = new System.Windows.Forms.PictureBox();
            this.wall_8 = new System.Windows.Forms.PictureBox();
            this.wall_7 = new System.Windows.Forms.PictureBox();
            this.wall_6 = new System.Windows.Forms.PictureBox();
            this.wall_5 = new System.Windows.Forms.PictureBox();
            this.wall_4 = new System.Windows.Forms.PictureBox();
            this.wall_3 = new System.Windows.Forms.PictureBox();
            this.wall_2 = new System.Windows.Forms.PictureBox();
            this.wall_1 = new System.Windows.Forms.PictureBox();
            this.ball = new System.Windows.Forms.PictureBox();
            this.racket = new System.Windows.Forms.PictureBox();
            this.lives_lbl = new System.Windows.Forms.Label();
            this.label_lives = new System.Windows.Forms.Label();
            this.time_lbl = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.points_lbl = new System.Windows.Forms.Label();
            this.label_score = new System.Windows.Forms.Label();
            this.playground.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wall_15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.racket)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick_1);
            // 
            // timer_time
            // 
            this.timer_time.Enabled = true;
            this.timer_time.Interval = 1000;
            this.timer_time.Tick += new System.EventHandler(this.timer_time_Tick);
            // 
            // playground
            // 
            this.playground.BackColor = System.Drawing.SystemColors.HighlightText;
            this.playground.Controls.Add(this.scr_lbl);
            this.playground.Controls.Add(this.your_scr_lbl);
            this.playground.Controls.Add(this.win_lbl);
            this.playground.Controls.Add(this.Sorry_lbl);
            this.playground.Controls.Add(this.Menu);
            this.playground.Controls.Add(this.New_gm);
            this.playground.Controls.Add(this.wall_15);
            this.playground.Controls.Add(this.wall_14);
            this.playground.Controls.Add(this.wall_13);
            this.playground.Controls.Add(this.wall_12);
            this.playground.Controls.Add(this.wall_11);
            this.playground.Controls.Add(this.wall_10);
            this.playground.Controls.Add(this.wall_9);
            this.playground.Controls.Add(this.wall_8);
            this.playground.Controls.Add(this.wall_7);
            this.playground.Controls.Add(this.wall_6);
            this.playground.Controls.Add(this.wall_5);
            this.playground.Controls.Add(this.wall_4);
            this.playground.Controls.Add(this.wall_3);
            this.playground.Controls.Add(this.wall_2);
            this.playground.Controls.Add(this.wall_1);
            this.playground.Controls.Add(this.ball);
            this.playground.Controls.Add(this.racket);
            this.playground.Controls.Add(this.lives_lbl);
            this.playground.Controls.Add(this.label_lives);
            this.playground.Controls.Add(this.time_lbl);
            this.playground.Controls.Add(this.label_time);
            this.playground.Controls.Add(this.points_lbl);
            this.playground.Controls.Add(this.label_score);
            this.playground.Location = new System.Drawing.Point(0, 0);
            this.playground.Name = "playground";
            this.playground.Size = new System.Drawing.Size(743, 446);
            this.playground.TabIndex = 0;
            this.playground.Paint += new System.Windows.Forms.PaintEventHandler(this.playground_Paint);
            // 
            // scr_lbl
            // 
            this.scr_lbl.AutoSize = true;
            this.scr_lbl.Font = new System.Drawing.Font("Mistral", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scr_lbl.Location = new System.Drawing.Point(520, 275);
            this.scr_lbl.Name = "scr_lbl";
            this.scr_lbl.Size = new System.Drawing.Size(31, 38);
            this.scr_lbl.TabIndex = 28;
            this.scr_lbl.Text = "0";
            this.scr_lbl.Visible = false;
            // 
            // your_scr_lbl
            // 
            this.your_scr_lbl.AutoSize = true;
            this.your_scr_lbl.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.your_scr_lbl.Location = new System.Drawing.Point(365, 272);
            this.your_scr_lbl.Name = "your_scr_lbl";
            this.your_scr_lbl.Size = new System.Drawing.Size(139, 42);
            this.your_scr_lbl.TabIndex = 27;
            this.your_scr_lbl.Text = "Your score:";
            this.your_scr_lbl.Visible = false;
            // 
            // win_lbl
            // 
            this.win_lbl.AutoSize = true;
            this.win_lbl.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.win_lbl.Location = new System.Drawing.Point(294, 247);
            this.win_lbl.Name = "win_lbl";
            this.win_lbl.Size = new System.Drawing.Size(237, 42);
            this.win_lbl.TabIndex = 26;
            this.win_lbl.Text = "You are the winner)";
            this.win_lbl.Visible = false;
            // 
            // Sorry_lbl
            // 
            this.Sorry_lbl.AutoSize = true;
            this.Sorry_lbl.Font = new System.Drawing.Font("Mistral", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Sorry_lbl.Location = new System.Drawing.Point(277, 227);
            this.Sorry_lbl.Name = "Sorry_lbl";
            this.Sorry_lbl.Size = new System.Drawing.Size(254, 44);
            this.Sorry_lbl.TabIndex = 25;
            this.Sorry_lbl.Text = "Sorry, but you lose(";
            this.Sorry_lbl.Visible = false;
            // 
            // Menu
            // 
            this.Menu.Location = new System.Drawing.Point(20, 297);
            this.Menu.Name = "Menu";
            this.Menu.Size = new System.Drawing.Size(80, 33);
            this.Menu.TabIndex = 24;
            this.Menu.Text = "Menu";
            this.Menu.UseVisualStyleBackColor = true;
            this.Menu.Visible = false;
            this.Menu.Click += new System.EventHandler(this.Menu_Click);
            // 
            // New_gm
            // 
            this.New_gm.Location = new System.Drawing.Point(20, 247);
            this.New_gm.Name = "New_gm";
            this.New_gm.Size = new System.Drawing.Size(80, 33);
            this.New_gm.TabIndex = 23;
            this.New_gm.Text = "New game";
            this.New_gm.UseVisualStyleBackColor = true;
            this.New_gm.Visible = false;
            this.New_gm.Click += new System.EventHandler(this.New_gm_Click);
            // 
            // wall_15
            // 
            this.wall_15.BackColor = System.Drawing.Color.LightBlue;
            this.wall_15.Location = new System.Drawing.Point(608, 137);
            this.wall_15.Name = "wall_15";
            this.wall_15.Size = new System.Drawing.Size(107, 28);
            this.wall_15.TabIndex = 22;
            this.wall_15.TabStop = false;
            // 
            // wall_14
            // 
            this.wall_14.BackColor = System.Drawing.Color.LightBlue;
            this.wall_14.Location = new System.Drawing.Point(457, 137);
            this.wall_14.Name = "wall_14";
            this.wall_14.Size = new System.Drawing.Size(107, 28);
            this.wall_14.TabIndex = 21;
            this.wall_14.TabStop = false;
            // 
            // wall_13
            // 
            this.wall_13.BackColor = System.Drawing.Color.LightBlue;
            this.wall_13.Location = new System.Drawing.Point(312, 137);
            this.wall_13.Name = "wall_13";
            this.wall_13.Size = new System.Drawing.Size(107, 28);
            this.wall_13.TabIndex = 20;
            this.wall_13.TabStop = false;
            // 
            // wall_12
            // 
            this.wall_12.BackColor = System.Drawing.Color.LightBlue;
            this.wall_12.Location = new System.Drawing.Point(159, 137);
            this.wall_12.Name = "wall_12";
            this.wall_12.Size = new System.Drawing.Size(107, 28);
            this.wall_12.TabIndex = 19;
            this.wall_12.TabStop = false;
            // 
            // wall_11
            // 
            this.wall_11.BackColor = System.Drawing.Color.LightBlue;
            this.wall_11.Location = new System.Drawing.Point(12, 137);
            this.wall_11.Name = "wall_11";
            this.wall_11.Size = new System.Drawing.Size(107, 28);
            this.wall_11.TabIndex = 18;
            this.wall_11.TabStop = false;
            // 
            // wall_10
            // 
            this.wall_10.BackColor = System.Drawing.Color.Blue;
            this.wall_10.Location = new System.Drawing.Point(608, 90);
            this.wall_10.Name = "wall_10";
            this.wall_10.Size = new System.Drawing.Size(107, 28);
            this.wall_10.TabIndex = 17;
            this.wall_10.TabStop = false;
            // 
            // wall_9
            // 
            this.wall_9.BackColor = System.Drawing.Color.Blue;
            this.wall_9.Location = new System.Drawing.Point(457, 90);
            this.wall_9.Name = "wall_9";
            this.wall_9.Size = new System.Drawing.Size(107, 28);
            this.wall_9.TabIndex = 16;
            this.wall_9.TabStop = false;
            // 
            // wall_8
            // 
            this.wall_8.BackColor = System.Drawing.Color.Blue;
            this.wall_8.Location = new System.Drawing.Point(312, 90);
            this.wall_8.Name = "wall_8";
            this.wall_8.Size = new System.Drawing.Size(107, 28);
            this.wall_8.TabIndex = 15;
            this.wall_8.TabStop = false;
            // 
            // wall_7
            // 
            this.wall_7.BackColor = System.Drawing.Color.Blue;
            this.wall_7.Location = new System.Drawing.Point(159, 90);
            this.wall_7.Name = "wall_7";
            this.wall_7.Size = new System.Drawing.Size(107, 28);
            this.wall_7.TabIndex = 14;
            this.wall_7.TabStop = false;
            // 
            // wall_6
            // 
            this.wall_6.BackColor = System.Drawing.Color.Blue;
            this.wall_6.Location = new System.Drawing.Point(12, 90);
            this.wall_6.Name = "wall_6";
            this.wall_6.Size = new System.Drawing.Size(107, 28);
            this.wall_6.TabIndex = 13;
            this.wall_6.TabStop = false;
            // 
            // wall_5
            // 
            this.wall_5.BackColor = System.Drawing.Color.DarkBlue;
            this.wall_5.Location = new System.Drawing.Point(608, 42);
            this.wall_5.Name = "wall_5";
            this.wall_5.Size = new System.Drawing.Size(107, 28);
            this.wall_5.TabIndex = 12;
            this.wall_5.TabStop = false;
            // 
            // wall_4
            // 
            this.wall_4.BackColor = System.Drawing.Color.DarkBlue;
            this.wall_4.Location = new System.Drawing.Point(457, 42);
            this.wall_4.Name = "wall_4";
            this.wall_4.Size = new System.Drawing.Size(107, 28);
            this.wall_4.TabIndex = 11;
            this.wall_4.TabStop = false;
            // 
            // wall_3
            // 
            this.wall_3.BackColor = System.Drawing.Color.DarkBlue;
            this.wall_3.Location = new System.Drawing.Point(312, 42);
            this.wall_3.Name = "wall_3";
            this.wall_3.Size = new System.Drawing.Size(107, 28);
            this.wall_3.TabIndex = 10;
            this.wall_3.TabStop = false;
            // 
            // wall_2
            // 
            this.wall_2.BackColor = System.Drawing.Color.DarkBlue;
            this.wall_2.Location = new System.Drawing.Point(159, 42);
            this.wall_2.Name = "wall_2";
            this.wall_2.Size = new System.Drawing.Size(107, 28);
            this.wall_2.TabIndex = 9;
            this.wall_2.TabStop = false;
            // 
            // wall_1
            // 
            this.wall_1.BackColor = System.Drawing.Color.DarkBlue;
            this.wall_1.Location = new System.Drawing.Point(12, 42);
            this.wall_1.Name = "wall_1";
            this.wall_1.Size = new System.Drawing.Size(107, 28);
            this.wall_1.TabIndex = 8;
            this.wall_1.TabStop = false;
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.DarkBlue;
            this.ball.Location = new System.Drawing.Point(129, 182);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(20, 20);
            this.ball.TabIndex = 7;
            this.ball.TabStop = false;
            // 
            // racket
            // 
            this.racket.BackColor = System.Drawing.Color.LightBlue;
            this.racket.Location = new System.Drawing.Point(314, 412);
            this.racket.Name = "racket";
            this.racket.Size = new System.Drawing.Size(112, 24);
            this.racket.TabIndex = 6;
            this.racket.TabStop = false;
            // 
            // lives_lbl
            // 
            this.lives_lbl.AutoSize = true;
            this.lives_lbl.Location = new System.Drawing.Point(672, 16);
            this.lives_lbl.Name = "lives_lbl";
            this.lives_lbl.Size = new System.Drawing.Size(13, 13);
            this.lives_lbl.TabIndex = 5;
            this.lives_lbl.Text = "3";
            // 
            // label_lives
            // 
            this.label_lives.AutoSize = true;
            this.label_lives.Location = new System.Drawing.Point(634, 15);
            this.label_lives.Name = "label_lives";
            this.label_lives.Size = new System.Drawing.Size(32, 13);
            this.label_lives.TabIndex = 4;
            this.label_lives.Text = "Lives";
            // 
            // time_lbl
            // 
            this.time_lbl.AutoSize = true;
            this.time_lbl.Location = new System.Drawing.Point(347, 15);
            this.time_lbl.Name = "time_lbl";
            this.time_lbl.Size = new System.Drawing.Size(25, 13);
            this.time_lbl.TabIndex = 3;
            this.time_lbl.Text = "120";
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Location = new System.Drawing.Point(311, 15);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(30, 13);
            this.label_time.TabIndex = 2;
            this.label_time.Text = "Time";
            // 
            // points_lbl
            // 
            this.points_lbl.AutoSize = true;
            this.points_lbl.Location = new System.Drawing.Point(58, 12);
            this.points_lbl.Name = "points_lbl";
            this.points_lbl.Size = new System.Drawing.Size(13, 13);
            this.points_lbl.TabIndex = 1;
            this.points_lbl.Text = "0";
            // 
            // label_score
            // 
            this.label_score.AutoSize = true;
            this.label_score.Location = new System.Drawing.Point(17, 12);
            this.label_score.Name = "label_score";
            this.label_score.Size = new System.Drawing.Size(35, 13);
            this.label_score.TabIndex = 0;
            this.label_score.Text = "Score";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 448);
            this.Controls.Add(this.playground);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Game";
            this.Text = "Game";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Game_KeyDown_1);
            this.playground.ResumeLayout(false);
            this.playground.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wall_15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wall_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.racket)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Timer timer_time;
        private System.Windows.Forms.Panel playground;
        private System.Windows.Forms.Label lives_lbl;
        private System.Windows.Forms.Label label_lives;
        private System.Windows.Forms.Label time_lbl;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label points_lbl;
        private System.Windows.Forms.Label label_score;
        private System.Windows.Forms.PictureBox racket;
        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.PictureBox wall_15;
        private System.Windows.Forms.PictureBox wall_14;
        private System.Windows.Forms.PictureBox wall_13;
        private System.Windows.Forms.PictureBox wall_12;
        private System.Windows.Forms.PictureBox wall_11;
        private System.Windows.Forms.PictureBox wall_10;
        private System.Windows.Forms.PictureBox wall_9;
        private System.Windows.Forms.PictureBox wall_8;
        private System.Windows.Forms.PictureBox wall_7;
        private System.Windows.Forms.PictureBox wall_6;
        private System.Windows.Forms.PictureBox wall_5;
        private System.Windows.Forms.PictureBox wall_4;
        private System.Windows.Forms.PictureBox wall_3;
        private System.Windows.Forms.PictureBox wall_2;
        private System.Windows.Forms.PictureBox wall_1;
        private System.Windows.Forms.Button Menu;
        private System.Windows.Forms.Button New_gm;
        private System.Windows.Forms.Label scr_lbl;
        private System.Windows.Forms.Label your_scr_lbl;
        private System.Windows.Forms.Label win_lbl;
        private System.Windows.Forms.Label Sorry_lbl;
    }
}