﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arcanoid
{
    public partial class Arcanoid : Form
    {
        public Arcanoid()
        {
            InitializeComponent();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Exit exit = new Exit();
            exit.Show();
        }

        private void instruction_Click(object sender, EventArgs e)
        {
            Instruction instr = new Instruction();
            instr.Show(); 
        }

        private void game_Click(object sender, EventArgs e)
        {
            Game game = new Game(this);
            game.Show();
        }
    }
}
