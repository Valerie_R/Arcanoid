﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arcanoid
{
    public partial class Game : Form
    {
        public int speed_gorizont = 2;
        public int speed_top = 2;
        public int point = 0;
        public int lives = 3;
        public int tm = 120;
        Arcanoid arcanoid;
        public Game(Arcanoid arcanoid)
        {
            InitializeComponent();
            timer.Enabled = true;
            racket.Top = playground.Bottom - (playground.Bottom / 10);
            ball.Paint += new PaintEventHandler(ball_Paint);
            this.arcanoid = arcanoid;
        }
        //Делаю ball круглым
        private void ball_Paint(object sender, PaintEventArgs e)
        {
            System.Drawing.Drawing2D.GraphicsPath shape = new System.Drawing.Drawing2D.GraphicsPath();
            shape.AddEllipse(0, 0, ball.Width, ball.Height);
            ball.Region = new Region(shape);
        }

        private void New_gm_Click(object sender, EventArgs e)
        {
            wall_1.Visible = true; wall_1.BackColor = Color.DarkBlue;
            wall_2.Visible = true; wall_2.BackColor = Color.DarkBlue;
            wall_3.Visible = true; wall_3.BackColor = Color.DarkBlue;
            wall_4.Visible = true; wall_4.BackColor = Color.DarkBlue;
            wall_5.Visible = true; wall_5.BackColor = Color.DarkBlue;
            wall_6.Visible = true; wall_6.BackColor = Color.Blue;
            wall_7.Visible = true; wall_7.BackColor = Color.Blue;
            wall_8.Visible = true; wall_8.BackColor = Color.Blue;
            wall_9.Visible = true; wall_9.BackColor = Color.Blue;
            wall_10.Visible = true; wall_10.BackColor = Color.Blue;
            wall_11.Visible = true;
            wall_12.Visible = true;
            wall_13.Visible = true;
            wall_14.Visible = true;
            wall_15.Visible = true;
            ball.Top = 250;
            ball.Left = 150;
            speed_gorizont = 2;
            speed_top = 2;
            timer.Enabled = true;
            New_gm.Visible = false;
            Menu.Visible = false;
            Sorry_lbl.Visible = false;
            your_scr_lbl.Visible = false;
            scr_lbl.Visible = false;
            win_lbl.Visible = false;
            lives = 3;
            lives_lbl.Text = lives.ToString();
            tm = 120;
            time_lbl.Text = tm.ToString();
        }

        private void Menu_Click(object sender, EventArgs e)
        {
            Close();
        }

        //гарячие клавиши
        private void timer_time_Tick(object sender, EventArgs e)
        {
            timer_time.Enabled = true;
            tm -= 1;
            time_lbl.Text = tm.ToString();
            if (tm == 0)
            {
                timer.Enabled = false;
                timer_time.Enabled = false;
                New_gm.Visible = true;
                Menu.Visible = true;
                Sorry_lbl.Visible = true;
                your_scr_lbl.Visible = true;
                scr_lbl.Visible = true;
                scr_lbl.Text = point.ToString();
            }
            if (timer.Enabled == false) timer_time.Enabled = false;
        }

        private void timer_Tick_1(object sender, EventArgs e)
        {
            racket.Left = Cursor.Position.X;
            ball.Left += speed_gorizont;
            ball.Top += speed_top;

            //координаты центра ball и racket
            Point ball_center = new Point((ball.Left + ball.Right) / 2, (ball.Top + ball.Bottom) / 2);
            Point racket_center = new Point((racket.Left + racket.Right) / 2, (racket.Top + racket.Bottom) / 2);

            if (ball.Bottom >= racket.Top && ball.Right >= racket.Left && ball.Left <= racket.Right)
            {
                //изменение скорости при ударение об racket
                if (ball_center.X <= racket_center.X - 20) speed_gorizont = speed_gorizont - 1;
                else if (ball_center.X >= racket_center.X + 20) speed_gorizont = speed_gorizont + 1;
                if (speed_gorizont == 4) speed_gorizont = 3;
                if (speed_gorizont == -4) speed_gorizont = -3;
            }

            //Отбивание от стен
            if (ball.Left <= playground.Left)
                speed_gorizont = -speed_gorizont;
            if (ball.Right >= playground.Right)
                speed_gorizont = -speed_gorizont;
            if (ball.Top <= playground.Top)
                speed_top = -speed_top;
            if (ball.Bottom >= playground.Bottom)
            {
                lives = lives - 1;
                lives_lbl.Text = lives.ToString();
                timer.Enabled = false;
                if (lives == 0)
                {
                    New_gm.Visible = true;
                    Menu.Visible = true;
                    Sorry_lbl.Visible = true;
                    your_scr_lbl.Visible = true;
                    scr_lbl.Visible = true;
                    scr_lbl.Text = point.ToString();
                }
            }
            //  создания массива PictureBox и в цикле прописываю отбивание от блоков
            PictureBox[] wall = { wall_1, wall_2, wall_3, wall_4, wall_5, wall_6, wall_7, wall_8, wall_9, wall_10, wall_11, wall_12, wall_13, wall_14, wall_15, racket };
            for (int i = 0; i < 16; i++)
            {
                if (wall[i].Visible == true)
                {
                    if (ball.Top <= wall[i].Bottom && ball.Right >= wall[i].Left && ball.Left <= wall[i].Right && ball.Bottom >= wall[i].Top)
                    {
                        if (((ball.Right == wall[i].Left && ball.Bottom == wall[i].Top) || (ball.Left == wall[i].Right && ball.Top == wall[i].Bottom)) ||
                            ((ball.Right == wall[i].Left && ball.Bottom == wall[i].Top + 1) || (ball.Left == wall[i].Right && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Right == wall[i].Left && ball.Bottom == wall[i].Top + 2) || (ball.Left == wall[i].Right && ball.Top == wall[i].Bottom - 2)) ||

                            ((ball.Right == wall[i].Left + 1 && ball.Bottom == wall[i].Top) || (ball.Left == wall[i].Right - 1 && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Right == wall[i].Left + 1 && ball.Bottom == wall[i].Top + 1) || (ball.Left == wall[i].Right - 1 && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Right == wall[i].Left + 1 && ball.Bottom == wall[i].Top + 2) || (ball.Left == wall[i].Right - 1 && ball.Top == wall[i].Bottom - 2)) ||

                            ((ball.Right == wall[i].Left + 2 && ball.Bottom == wall[i].Top) || (ball.Left == wall[i].Right - 2 && ball.Top == wall[i].Bottom - 2)) ||
                            ((ball.Right == wall[i].Left + 2 && ball.Bottom == wall[i].Top + 1) || (ball.Left == wall[i].Right - 2 && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Right == wall[i].Left + 2 && ball.Bottom == wall[i].Top + 2) || (ball.Left == wall[i].Right - 2 && ball.Top == wall[i].Bottom - 2))
                            && speed_gorizont * speed_top < 0)
                        {
                            if (ball.Top <= wall[i].Top)
                            {
                                speed_gorizont = -Math.Abs(speed_gorizont);
                                speed_top = -Math.Abs(speed_top);
                            }
                            else
                            {
                                speed_gorizont = Math.Abs(speed_gorizont);
                                speed_top = Math.Abs(speed_top);
                            }
                        }
                        else
                        if (((ball.Left == wall[i].Right && ball.Bottom == wall[i].Top) || (ball.Right == wall[i].Left && ball.Top == wall[i].Bottom)) ||
                            ((ball.Left == wall[i].Right && ball.Bottom == wall[i].Top + 1) || (ball.Right == wall[i].Left && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Left == wall[i].Right && ball.Bottom == wall[i].Top + 2) || (ball.Right == wall[i].Left && ball.Top == wall[i].Bottom - 2)) ||

                            ((ball.Left == wall[i].Right - 1 && ball.Bottom == wall[i].Top) || (ball.Right == wall[i].Left + 1 && ball.Top == wall[i].Bottom)) ||
                            ((ball.Left == wall[i].Right - 1 && ball.Bottom == wall[i].Top + 1) || (ball.Right == wall[i].Left + 1 && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Left == wall[i].Right - 1 && ball.Bottom == wall[i].Top + 2) || (ball.Right == wall[i].Left + 1 && ball.Top == wall[i].Bottom - 2)) ||

                            ((ball.Left == wall[i].Right - 2 && ball.Bottom == wall[i].Top) || (ball.Right == wall[i].Left + 2 && ball.Top == wall[i].Bottom)) ||
                            ((ball.Left == wall[i].Right - 2 && ball.Bottom == wall[i].Top + 1) || (ball.Right == wall[i].Left + 2 && ball.Top == wall[i].Bottom - 1)) ||
                            ((ball.Left == wall[i].Right - 2 && ball.Bottom == wall[i].Top + 2) || (ball.Right == wall[i].Left + 2 && ball.Top == wall[i].Bottom - 2))
                            && speed_gorizont * speed_top > 0)
                        {
                            if (ball.Top <= wall[i].Top)
                            {
                                speed_gorizont = Math.Abs(speed_gorizont);
                                speed_top = -Math.Abs(speed_top);
                            }
                            else
                            {
                                speed_gorizont = -Math.Abs(speed_gorizont);
                                speed_top = Math.Abs(speed_top);
                            }
                        }
                        else
                        if (ball.Right == wall[i].Left || ball.Right == wall[i].Left + 1 || ball.Right == wall[i].Left + 2) speed_gorizont = -speed_gorizont;
                        else
                        if (ball.Left == wall[i].Right || ball.Left == wall[i].Right - 1 || ball.Left == wall[i].Right - 2) speed_gorizont = -speed_gorizont;
                        else
                        if (ball.Top == wall[i].Bottom || ball.Top == wall[i].Bottom - 1 || ball.Top == wall[i].Bottom - 2) speed_top = -speed_top;
                        else
                        if (ball.Bottom == wall[i].Top || ball.Bottom == wall[i].Top + 1 || ball.Bottom == wall[i].Top + 2) speed_top = -speed_top;

                        point += 1;
                        points_lbl.Text = point.ToString();

                        if (wall[i].BackColor == Color.DarkBlue) wall[i].BackColor = Color.Blue;
                        else if (wall[i].BackColor == Color.Blue) wall[i].BackColor = Color.LightBlue;
                        else if (wall[i] == racket) ;
                        else wall[i].Visible = false;
                    }
                }
            }

            if (wall_1.Visible == false && wall_2.Visible == false && wall_3.Visible == false
                && wall_4.Visible == false && wall_5.Visible == false && wall_6.Visible == false
                && wall_7.Visible == false && wall_8.Visible == false && wall_9.Visible == false
                && wall_10.Visible == false && wall_11.Visible == false && wall_12.Visible == false
                && wall_13.Visible == false && wall_14.Visible == false && wall_15.Visible == false)
            {
                New_gm.Visible = true;
                Menu.Visible = true;
                timer.Enabled = false;
                win_lbl.Visible = true; 
            }
            if (timer.Enabled == true) timer_time.Enabled = true;
        }

        private void Game_KeyDown_1(object sender, KeyEventArgs e)
        {
            this.KeyPreview = true;
            if (e.KeyCode == Keys.Escape) this.Close();
            if (e.KeyCode == Keys.F1) timer.Enabled = false;
            if (e.KeyCode == Keys.F2) timer.Enabled = true;
            if (e.KeyCode == Keys.Space)
            {
                New_gm_Click(sender, e);                   // button_Click кнопка по которой кликнуть надо
                e.Handled = true;
            }

            if (timer.Enabled == false)
            {
                if (e.KeyCode == Keys.F3)
                {

                    ball.Top = 250;
                    ball.Left = 150;
                    speed_gorizont = 2;
                    speed_top = 2;
                    timer.Enabled = true;
                }
            }
        }

        private void playground_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
