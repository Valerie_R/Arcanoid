﻿namespace Arcanoid
{
    partial class Exit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exit));
            this.label1 = new System.Windows.Forms.Label();
            this.exit_yes = new System.Windows.Forms.Button();
            this.exit_no = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Mistral", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(332, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Do you really want to exit?";
            // 
            // exit_yes
            // 
            this.exit_yes.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit_yes.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.exit_yes.Location = new System.Drawing.Point(81, 95);
            this.exit_yes.Name = "exit_yes";
            this.exit_yes.Size = new System.Drawing.Size(71, 43);
            this.exit_yes.TabIndex = 1;
            this.exit_yes.Text = "Yes";
            this.exit_yes.UseVisualStyleBackColor = true;
            this.exit_yes.Click += new System.EventHandler(this.exit_yes_Click);
            // 
            // exit_no
            // 
            this.exit_no.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit_no.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.exit_no.Location = new System.Drawing.Point(198, 96);
            this.exit_no.Name = "exit_no";
            this.exit_no.Size = new System.Drawing.Size(71, 42);
            this.exit_no.TabIndex = 2;
            this.exit_no.Text = "No";
            this.exit_no.UseVisualStyleBackColor = true;
            this.exit_no.Click += new System.EventHandler(this.exit_no_Click);
            // 
            // Exit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(354, 156);
            this.Controls.Add(this.exit_no);
            this.Controls.Add(this.exit_yes);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Exit";
            this.Text = "Exit";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button exit_yes;
        private System.Windows.Forms.Button exit_no;
    }
}