﻿namespace Arcanoid
{
    partial class Arcanoid
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Arcanoid));
            this.label1 = new System.Windows.Forms.Label();
            this.game = new System.Windows.Forms.Button();
            this.instruction = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Mistral", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(53, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 76);
            this.label1.TabIndex = 0;
            this.label1.Text = "Arcanoid";
            // 
            // game
            // 
            this.game.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.game.Location = new System.Drawing.Point(101, 99);
            this.game.Name = "game";
            this.game.Size = new System.Drawing.Size(122, 35);
            this.game.TabIndex = 1;
            this.game.Text = "Play game";
            this.game.UseVisualStyleBackColor = true;
            this.game.Click += new System.EventHandler(this.game_Click);
            // 
            // instruction
            // 
            this.instruction.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.instruction.Location = new System.Drawing.Point(101, 155);
            this.instruction.Name = "instruction";
            this.instruction.Size = new System.Drawing.Size(122, 35);
            this.instruction.TabIndex = 2;
            this.instruction.Text = "Instruction";
            this.instruction.UseVisualStyleBackColor = true;
            this.instruction.Click += new System.EventHandler(this.instruction_Click);
            // 
            // exit
            // 
            this.exit.Font = new System.Drawing.Font("Cambria", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit.Location = new System.Drawing.Point(101, 216);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(122, 35);
            this.exit.TabIndex = 3;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // Arcanoid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(327, 289);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.instruction);
            this.Controls.Add(this.game);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Arcanoid";
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button game;
        private System.Windows.Forms.Button instruction;
        private System.Windows.Forms.Button exit;
    }
}

